#Write a function that accepts an integer n, n > 0, and returns a list of all n-digit prime numbers

def n_digit_primes(n):
    primes = []
    for i in range(10**(n-1),10**n):
       count = 0
       for j in range (2,i):
           if i%j == 0:
              count +=1
       if count == 0:
          primes.append(i)
    return primes
n =input()
print(n_digit_primes(n))

#Did you write a helper function? If not, write a helper function, is_prime that returns whether a number is prime or not, and use it in your n_digit_primes function
	

def is_prime(x):
    count = 0
    for j in range (2,x):
        if x%j == 0:
           count +=1
    return count == 0  

def n_digit_primes1(n):
    primes = []
    for i in range(10**(n-1),10**n):
        k = is_prime(i)
        if k == True:
           primes.append(i)
    return primes
n1 = input()
print(n_digit_primes1(n1))

