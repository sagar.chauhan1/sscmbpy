x1 = [3, 1, 2, 4, 1, 2]

#Find the sum of all the elements in x

print(sum(x1))

#Find the length of x

print(len(x1))

#Print the last three items of x

print(x1[-3:])

#Print the first three items of x

print(x1[:3])

#Sort x

x1.sort()
print(x1)

#Add another item, 10 to x

x1.append(10)
print(x1)

#Add another item 11 to x

x1.append(11)
print(x1)

#Remove the last item from x

print(x1.pop())

#How many times does 1 occur in x?

print(x1.count(1))

#Check whether the element 10 is present in x

if 10 in x1:
	print("yes")
else:
	print("No")

y1 = [5, 1, 2, 3]


#Add all the elements of y to x

print(x1+y1)

#Create a copy of x

z = x1
print(z)

#Can a list contain elements of different data-types?

#Ans    Yes

#Which data structure does Python use to implement a list?
#Ans   Stack

