def is_prime(x):
    count = 0
    for j in range (2,x):
        if x%j == 0:
           count +=1
    if count == 0:
       return 1
    else:
       return 0  

def n_digit_primes(n):
    primes = []
    for i in range(10**(n-1),10**n):
        k = is_prime(i)
        if k == 1:
           primes.append(i)
    return primes
print(n_digit_primes(2))

#Call the n_digit_primes with a keyword argument, n_digit_primes(n=1)

def is_prime(x):
    count = 0
    for j in range (2,x):
        if x%j == 0:
           count +=1
    if count == 0:
       return 1
    else:
       return 0  

def n_digit_primes(n=1):
    primes = []
    for i in range(10**(n-1),10**n):
        k = is_prime(i)
        if k == 1:
           primes.append(i)
    return primes
print(n_digit_primes(2))
print(n_digit_primes())

#Define a function, args_sum that accepts an arbitrary number of integers as input and computes their sum.

def sumab(*s):
	print(sum(s))
sum_absolute(1,2,3,4,5,6,7)
#Modify the args_sum function so that it accepts an optional, boolean, keyword argument named absolute. If absolute is True, then args_sum must return the absolute value of the sum of *args. If absolute is not specified, then return the sum without performing any conversion.

import math
def args_sum(*args, **kwargs):
	total = sum(args)
	if kwargs.get('absolute') is True:
		return abs(total)
	else:
		return total

a = args_sum(1,2,-3.5,-4,absolute = True)
b = args_sum(1,2.5,absolute = "")

print(a,"  ",b)


