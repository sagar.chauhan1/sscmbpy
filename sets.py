'''
https://conceptcoaster.com/course/python-tutorial/sets/

Define a function that does the equivalent of uniq <file_path> | sort, i.e., the function should

Accept a file path as the argument
Read the contents of the file into a string
Split the string into lines
Remove duplicate lines
Sort the lines
Return the sorted lines
'''
def fileread(file_path):
	f = open(file_path,"r")
	s = f.read()
	print s
    lines = s.split("\n")
    unique_lines = set(s1)
    return sorted(unique_lines)
    
file_path = raw_input()
fileread(file_path)
