
x = "one two three four"

#Print the last 3 characters of x

print("Last 3 charactes",x[-3:])		#('Last 3 charactes', 'our')

#Print the first 10 characters of x
print("First 10 charactes",x[:10])		#('First 10 charactes', 'one two th')

#Print characetrs 4 through 10 of x
print("ch 4 to 10 ",x[3:10])			#('ch 4 to 10 ', ' two th')

#Find the length of x
print(len(x))					#18

#Split x into its words
q = x.split(" ")
print(q)					#['one', 'two', 'three', 'four']

#Capitalize the first character of x
print(x.capitalize())				#One two three four

#Convert x into uppercase
print(x.upper())				#ONE TWO THREE FOUR

y = x
x = "one two three"
#After executing the above code, what is the value of y?
print(y)					#one two three four
