import csv
def numbers_and_squares(n, file_path):  
	l = []
	for i in range(1,n+1):
		l.append([i,pow(i,2)]) 
	with open(file_path, 'w') as writeFile:
    		writer = csv.writer(writeFile)
    		writer.writerows(l)

x = "a1.csv"
n = input("enter the number")
numbers_and_squares(n,x) 
